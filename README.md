/_ Practicing the way of awareness Gives rise to benefit without limit. We vow
to share the fruits with all beings. We vow to offer tribute to parents,
teacherse, friends. And numerous beings Who give guidance and support along the
path. - Thich Nhat Hanh _/

This readme is for Student Bulk Uploader Made for TestRocket Portal.

Steps :-

1. Download or clone the code from gitlab repository.

2. Run the index.html in your browser using a local server. (Tutorial Link
   below)

3. In the web page, Login with the institute credentials of which you are adding
   students. Then, it will redirect you to another page. Click on the file
   button and select the student data csv file. Please make sure headings of csv
   are correct. For a demo file, check link below. (Required fields- > Fullname,
   Mobile, Email, Password, Batch)

4. Click Load CSV button after. it will display the student data in table
   format. Verify that student data is reflecting here properly.

5. Open the config.json, and inside batches braces -> add batch_name from csv
   with its id if batch_name is not already present there.

6. Click on start upload button.

7. In case if student upload fails, check the reason for failure. Create a new
   csv with failed students and correction, and repeat from 3rd step till there
   are no fails.

Faq:-

Q. How do you set up a local server? - A.
https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server

Q. After loading csv, table is showing undefined in a column which is present in
csv file? A. Please ensure heading of the column is correct and there are no
space in the end or beginning.

Q. Demo File Format? A.
https://docs.zoho.com/file/4jl1g57bcc6bfbcf2461caf8a18cf763ac0ca
