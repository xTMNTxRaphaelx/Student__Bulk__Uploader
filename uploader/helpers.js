/**
 * Helper Function which creates a error message object.
 * @param {String} msg Error Message
 */
function errorMessage(msg) {
    return {
        status: 'error',
        msg: msg
    }
}

function makeTable(domId, dataArray, tableHeaders) {
    // Header Element
    $(domId).append(
        '<thead>' +
        $.map(tableHeaders, function (header, i) {
            return '<th>' + header + '</th>';
        }).join() +
        '</thead>'
    )
    // Body part - functional programming so cool
    $(domId).append(
        '<tbody>' +
        $.map(dataArray, function (data, i) {
            // each row
            return (
                '<tr data-srid="'+data["S.No."]+'">' +
                $.map(tableHeaders, function (header, i) {
                    // each column
                    return '<td>'+data[header]+'</td>';
                }).join() +
                '</tr>'
            )
        }).join() +
        '</tbody>'
    )
}