"use strict";

/**
 * This function initialize uploading of students one by one through self-recursion.
 * Checks if all the required field are present, send a request and move on to next dataset then till dataset is completed.
 * @param {Array} studentsArray Array of objects containing all students data from CSV
 */
function initUploader(studentsArray, jsonMapping, batches) {
  var i = 0;
  uploadStudent(i);

  function uploadStudent(i) {
    var student = studentsArray[i];
    var srNo = student["S.No."];
    var rowEl = $('table tr[data-srid="' + srNo + '"');
    // check if mandatory field are there, only then continue forward
    if (
      student["Full Name"] &&
      student["Mobile"] &&
      student["Email"] &&
      student["Password"] &&
      student["Batch"]
    ) {
      $(rowEl).addClass("blue lighten-5");
      var studentInfo = Object.keys(student);
      var mappedStudentObject = {};
      for (var j = 0; j < studentInfo.length; j++) {
        // remove waste values
        if (
          studentInfo[j] &&
          student[studentInfo[j]] &&
          studentInfo[j] !== "S.No."
        ) {
          // convert batch name to batch id
          if (studentInfo[j] === "Batch") {
            mappedStudentObject[jsonMapping[studentInfo[j]]] =
              batches[student[studentInfo[j]]];
          } else {
            mappedStudentObject[jsonMapping[studentInfo[j]]] =
              student[studentInfo[j]];
          }
        }
      }
      save(mappedStudentObject, function(response) {
        $(rowEl).removeClass("blue");
        if (response.status !== "error") {
          $(rowEl).addClass("green lighten-5");
        } else {
          var err = JSON.parse(response.msg.responseText).message;
          $(rowEl)
            .find("td:last-child")
            .html(err);
          $(rowEl).addClass("red lighten-5");
        }
        if (i < studentsArray.length - 1) {
          i++;
          uploadStudent(i);
        }
      });
    } else {
      $(rowEl).addClass("red lighten-5");
      if (i < studentsArray.length - 1) {
        i++;
        uploadStudent(i);
      }
    }
  }
}

/**
 * This function saves the student info on database using Ajax POST request.
 * @param {Object} studentObject Student JSON Object to send in API request
 * @param {Function} cb Callback function which is called when request is completed[success/error]. Used for furthur recursion.
 */
function save(studentObject, cb) {
  $.ajax({
    url: "http://35.154.139.183:8080/institute_student",
    method: "POST",
    dataType: "json",
    data: JSON.stringify(studentObject),
    contentType: "application/json",
    headers: {
      Authorization: getCookie("authorization")
    },
    success: function(response) {
      cb(response);
    },
    error: function(err) {
      cb(errorMessage(err));
    }
  });
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
