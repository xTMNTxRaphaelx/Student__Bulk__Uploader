'use strict';
/**
 * 
 * @param {string} inputEl id of the input type file element
 * @param {Function} cb Function which returns when there is a success or failure
 */
function readFile(inputEl, cb) {
    var input, file, fr;
    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
        return errorMessage('The File APIs are not fully supported in this browser.');
    }

    input = document.getElementById(inputEl);

    if (!input) {
        return errorMessage("Um, couldn't find the fileinput element.");
    } else if (!input.files) {
        return errorMessage("This browser doesn't seem to support the `files` property of file inputs.");
    } else if (!input.files[0]) {
        return errorMessage("Please select a file before clicking 'Load'");
    } else {
        file = input.files[0];
        fr = new FileReader();
        fr.onload = cb;
        fr.readAsText(file);
    }
}